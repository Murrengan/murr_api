module gitlab.com/Murrengan/murr_api

go 1.16

require (
	github.com/golang/protobuf v1.5.2
	google.golang.org/genproto v0.0.0-20200526211855-cb27e3aa2013
	google.golang.org/grpc v1.44.0
	google.golang.org/protobuf v1.27.1
)
