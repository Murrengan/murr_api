.EXPORT_ALL_VARIABLES:
GOBIN = $(shell pwd)/bin
GO111MODULE = on
SHELL=/bin/bash

.PHONY: deps
deps:
	@go mod vendor
	@go mod tidy
	@./scripts/proto-third-party.sh

.PHONY: tools
tools:
	@cd tools ; ./install.sh

.PHONY: clean
clean:
	@rm -fv ./bin/*

.PHONY: generate
generate: deps tools
	@./scripts/generate.sh
